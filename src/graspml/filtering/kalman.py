"""
Kalman filters
"""


class KalmanFilter1d:
    """Implements the 1D Kalman filter

    .. note:: Source: https://www.kalmanfilter.net/kalman1d.html
    """

    def __init__(self, x_init, dx_init, p_init, q=0, r=0):
        """
        :param x_init: Initial guess of the estimated value
        :param dx_init: Initial guess of the derivative of the estimated value
        :param p_init: Initial estimate uncertainty
        :param q: Process noise variance
        :param r: Measurement uncertainty
        """
        self.x_hat = x_init
        self.dx_hat = dx_init
        self.p = p_init + q
        self.q = q
        self.r = r
        self.gain = []

    def update(self, z):
        """Update the filter with next measurement

        :param z: Measurement point
        :return: Estimation of the true value
        """
        # State extrapolation
        self.x_hat += self.dx_hat
        self.dx_hat = self.dx_hat

        # Kalman gain
        K = self.p / (self.p + self.r)

        # State update
        self.x_hat += K * (z - self.x_hat)

        # Covariance update
        self.p *= 1 - K

        # Extrapolated estimate uncertainty
        self.p += self.q

        # Store gain
        self.gain.append(K)

        return self.x_hat
