from collections import defaultdict, Counter
from numpy.typing import ArrayLike

import numpy as np

from graspml.tree import Node, apply


def counts(x, v):
    """Count occurrences of value v in array x."""
    return sum([c == v for c in x])


def majority(x: ArrayLike):
    """Return the majority value.

    Parameters
    ----------
    x : array-like
        Input array.

    Returns
    -------
    majority : Any
        Most frequent value in input array.

    .. [1] https://stackoverflow.com/questions/6252280/find-the-most-frequent-number-in
           -a-numpy-array
    """
    values, counts = np.unique(x, return_counts=True)
    idx = np.argmax(counts)

    majority = values[idx]

    return majority


def I(p, n):
    """Expected information."""
    if (p == 0) or (n == 0):
        return 0

    pi = p / (n + p)
    ni = n / (n + p)
    return -pi * np.log2(pi) - ni * np.log2(ni)


def gain(A, cls):
    """Information gain by branching."""
    n = counts(cls, 0)
    p = counts(cls, 1)

    if (n == 0) & (p == 0):
        return 0

    C = defaultdict(list)
    for a, c in zip(A, cls):
        C[a].append(c)
    C = [(counts(C[a], 0), counts(C[a], 1)) for a in C.keys()]

    E = sum([(p_i + n_i) / (p + n) * I(p_i, n_i) for n_i, p_i in C])

    return I(p, n) - E


class IterativeDichotomiser3:
    """Implementation of the ID3 algorithm.

    .. [1] J.R. Quinlan, "Induction of Decision Trees" Machine Learning, vol. 1,
           pp. 81-106, 1986
    """

    def __init__(self):
        self.features = None
        self.tree = None

    def fit(self, X: ArrayLike, y: ArrayLike):
        """Fit the attributes in X to the classes in y.

        Parameters
        ----------
        X : array-like of shape (n_samples, n_features)
            Train samples.
        y : array-like of shape (n_features)
            True labels (n_samples)
        """
        X = np.array(X)
        y = np.array(y)

        # Fix the feature id with its column index
        features = list(range(X.shape[1]))

        # Build the decision tree
        self.tree = self._id3(X, y, features)

    def predict(self, X: ArrayLike, y: ArrayLike = None) -> ArrayLike:
        """Predict the labels for the test samples.

        Parameters
        ----------
        X : array-like of shape (n_samples, n_features)
            Test samples.
        y : Ignored
            Not used, only for compatibility with scikit-learn.

        Returns
        -------
        y_pred : array-like
            Predicted labels.
        """
        y_pred = []
        for row in X:
            y_pred.append(apply(self.tree, row))

        return np.array(y_pred)

    def _id3(self, A: ArrayLike, cls: ArrayLike, attributes: ArrayLike):
        """Implements a recursive step of the iterative dichotomizer 3 algorithm.

        Parameters
        ----------

        A : array-like of shape (n_samples, n_features)
            Attributes
        cls : array-like of shape (n_samples)
            Classes
        attributes: array-like of shape (n_features)
            Attribute labels
        """

        if len(attributes) == 1:
            # Stop if only one attribute left
            return Node(name=attributes[0], is_leaf=True, value=majority(cls))

        if len(set(cls)) == 1:
            # Stop if only one class left
            return Node(name="", is_leaf=True, value=majority(cls))

        # Information gained by branching
        gains = [gain(A[:, attribute], cls) for attribute in attributes]

        # Attribute with the largest information gain
        idx_max = np.argmax(gains)
        attribute = attributes[idx_max]

        # Attribute values and discrete set of possible outcomes
        values = A[:, attribute]
        outcomes = np.unique(values)

        # New dataset and labels for each new branch
        subsets = [A[values == o] for o in outcomes]
        labels = [cls[values == o] for o in outcomes]

        # Remove used attribute
        attributes = np.delete(attributes, idx_max)

        node = Node(attribute, is_leaf=False, feature=attribute)
        for outcome, A, cls in zip(outcomes, subsets, labels):
            node.add_child(outcome, self._id3(A, cls, attributes))

        return node
