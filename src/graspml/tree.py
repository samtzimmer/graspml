"""
Tree data structure.
"""
from typing import TypeVar, Any, Hashable, Union

from numpy.typing import ArrayLike

TNode = TypeVar("TNode")


class Node:
    """Node of a tree.

    Implements a node in a tree that can either be a node with references to its
    children or a leaf node in which case it can return a value.

    Parameters
    ----------
    name : str
        Name of the node.
    is_leaf : bool
        The node is a leaf or not.
    feature : int
        The index of the feature in the data array.
    value : any
        The value of the leaf node.

    Raises
    ------
    ValueError
        If ``is_leaf`` is set but no value is provided.
    """

    def __init__(
        self, name: str, is_leaf: bool = False, feature: int = None, value: Any = None
    ):
        self.name = name
        self.is_leaf = is_leaf

        if ~is_leaf & (feature is None):
            raise ValueError("Feature cannot be none if Node is a leaf.")
        self.feature = feature

        if is_leaf & (value is None):
            raise ValueError("Value cannot be none if Node is a leaf.")

        self.value = value
        self.children = {}

    def add_child(self, edge: Hashable, child: TNode) -> TNode:
        """Add a child to the node.

        Parameters
        ----------
        edge : hashable
            Edge label is used as a decision parameter during traversing the tree.
        child : Node
            The child node.

        Raises
        ------
        AssertionError
            If Node is a leaf node.
        """
        assert not self.is_leaf, "Cannot assign a child to a leaf node."
        self.children[edge] = child

        return self

    def get_child(self, edge: Hashable) -> TNode:
        """Get a child from the node.

        Parameters
        ----------
        edge : hashable
            Edge label is used as a decision parameter during traversing the tree.
        """
        return self.children[edge]

    def __repr__(self):
        return f"Node({self.name}, {self.value if self.is_leaf else self.feature})"

    def __str__(self):
        return f"Node({self.name}, {self.value if self.is_leaf else self.feature}, {self.children if not self.is_leaf else None})"


def apply(node: Node, x: ArrayLike) -> Union[Any, Node]:
    """Apply a data array onto the tree.

    Parameters
    ----------
    node : Node
        Node
    x : array-like
        Array that is applied to the tree.
    """
    if node.is_leaf:
        return node.value

    val = x[node.feature]
    return apply(node.get_child(val), x)
