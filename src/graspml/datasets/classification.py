from collections import namedtuple

import numpy as np

Dataset = namedtuple("Dataset", field_names=["data", "labels", "name", "features"])


# Source: J.R. Quinlan (1986) Machine Learning 1, 81-106
saturday_morning = Dataset(
    data=np.array(
        [
            ["sunny", "hot", "high", "false"],
            ["sunny", "hot", "high", "true"],
            ["overcast", "hot", "high", "false"],
            ["rain", "mild", "high", "false"],
            ["rain", "cold", "normal", "false"],
            ["rain", "cold", "normal", "true"],
            ["overcast", "cold", "normal", "true"],
            ["sunny", "mild", "high", "false"],
            ["sunny", "cold", "normal", "false"],
            ["rain", "mild", "normal", "false"],
            ["sunny", "mild", "normal", "true"],
            ["overcast", "mild", "high", "true"],
            ["overcast", "hot", "normal", "false"],
            ["rain", "mild", "high", "true"],
        ]
    ),
    labels=np.array([0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0]),
    name="Saturday mornings dataset in Table 1",
    features=["outlook", "temperature", "humidity", "windy"],
)
